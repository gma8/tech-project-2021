## The objectives for Week 2 are

#### 1. Continue working on your Spring Boot REST API and UI applications.
This is the highest priority! A working REST API and minimal functionality in a web UI.

Try to focus on code quality rather than quantity.

Try to include unit tests and code coverage reports.

Consider adding some JavaDoc.

#### 2. Create a jenkins job to test and build your REST API.

#### 3. Update the jenkins job to build a docker image for your REST API.

#### 4. Deploy your UI to an AWS S3 bucket
The simplest way to initially deploy your UI files is to get them into a public S3 bucket. This is a scalable and reliable way to deliver static HTML/JS/CSS files.

When this is working, ask your instructor about creating a Jenkins pipeline to send the UI files to S3 automatically.

#### 5. Optionally update the jenkins job to continuously deploy the docker image to kubernetes/openshift.
This is optional, ask your instructor for help. An example Jenkins file that does this is here:
https://bitbucket.org/fcallaly/mongodb-rest-pokemon/src/master/Jenkinsfile.full

#### 6. Develop an application Roadmap.
What features would be added to this application? What would a fully featured UI look like?

This will form part of your Friday presentation.

You don't need to implement these features. This is a roadmap for what you would implement IF you were given more resources for this project.

You can bring in your expertise from other fields - some examples might be:

- Any ML features or components that you would consider.
- Any web services or cloud services that this system could interface with.
- Any interesting UI features.
- Integrations with other applications.
- Security concerns.

## Friday Presentations

On Friday afternoon each team will give a 15min presentation on your work, some ideas for this are:

* Your vision for the application.
* What you have written, you can walk through code snippets, explain the architecture.
* Project management - how you managed tasks.
* Revision control - your branching strategy - how did that work out for you.
* Your application roadmap - what would you implement for such an application.
* What you learned, found challenging, found rewarding etc.

Remember we will have 6 presentations on similar projects, so try to add a unique twist or angle to your presentation.

There will be 5 minutes for questions after each presentation.

All groups will be winners, however we will announce an official winner at the end of the day. We will have some independent judges from RBC with us.